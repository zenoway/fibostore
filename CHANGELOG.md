# Changelog

## 0.1 (2016-06-03)

Initial public release

- Fibonacci algorithm
- Cache and store Fibonacci sequence
- REST API GET request to return the Fibonacci sequence of a given length in JSON format
- Basic support of RedisDB
- Basic logging
- Basic Gunicorn support


