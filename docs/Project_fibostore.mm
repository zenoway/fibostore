<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1463645357612" ID="ID_869061558" MODIFIED="1463767782251" TEXT="Project: fibostore">
<node CREATED="1463648115372" ID="ID_1044260426" MODIFIED="1463648166656" POSITION="right" TEXT="What is Fibonacci numbers and what are its practical applications?">
<node CREATED="1463648171408" ID="ID_1621403406" MODIFIED="1463648174030" TEXT="https://en.wikipedia.org/wiki/Fibonacci_word"/>
<node CREATED="1463648181166" ID="ID_1010258253" MODIFIED="1463648181806" TEXT="https://www.quora.com/What-are-the-practical-applications-of-Fibonacci-numbers?share=1"/>
<node CREATED="1463648295556" ID="ID_1067966847" MODIFIED="1463648296653" TEXT="http://math.stackexchange.com/questions/381/applications-of-the-fibonacci-sequence"/>
<node CREATED="1463648350148" ID="ID_136633531" MODIFIED="1463648351427" TEXT="http://mathcs.holycross.edu/~groberts/Talks/FibTalk-web.pdf"/>
<node CREATED="1463648393420" ID="ID_915698893" MODIFIED="1463648394476" TEXT="https://www.quora.com/What-are-the-real-life-applications-of-Fibonacci-series?share=1"/>
</node>
<node CREATED="1463648461479" FOLDED="true" ID="ID_438757152" MODIFIED="1464614489656" POSITION="left" TEXT="Goals &amp; features">
<node CREATED="1463648476173" ID="ID_985148745" MODIFIED="1463659730198">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      1 The project should provide a RESTful web service.<br />&#160;a. The web service accepts a number, n, as input and returns the first n Fibonacci numbers, starting from 0. I.e. given n &#160;= 5, appropriate output would represent the sequence [0, 1, 1, 2, 3].<br />&#160;b. Given a negative number, it will respond with an appropriate error.<br /><br />2. Include whatever instructions are necessary to build and deploy/run the project, where &quot;deploy/run&quot; means the web service is accepting requests and responding to them as appropriate.<br /><br />3. Include some unit and/or functional tests &#160;&#160;&#160;<br /><br />4. Use any language that you know well<br /><br />While this project is admittedly trivial, approach it as representing a more complex problem that you'll have to put into production and maintain for 5 years.<br />Providing a link to a github/bitbucket repo with the project would probably be the easiest way to submit.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1463659664210" ID="ID_1298942306" MODIFIED="1464248399340" POSITION="left" TEXT="Whiteboard">
<node CREATED="1463659949144" ID="ID_469380651" MODIFIED="1463660001547">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Fibostore is a platform for calculating and storing Fibonacci numbers.
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1463660004608" ID="ID_220534179" MODIFIED="1463660877106">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Focused on performance: we cache running client requests to minimize calculations. When the system receives a request (via REST API) to return Fibonacci number for a given N, it checks the cache if such N is already being processed. In such a case, the system puts the client in queue (there might be multiple similar requests), and upon the completion of calculation sends the results to all clients in the queue.
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1463660030134" ID="ID_891162390" MODIFIED="1463660091945" TEXT="Minimized amount of computations by storing the results of previous requests"/>
<node CREATED="1463660094013" ID="ID_1699366216" MODIFIED="1463660211979" TEXT="With time goes by, we can predict that the database will grow and we will need rather to access database to return the results, than do calculations."/>
<node CREATED="1463660219463" ID="ID_554162871" MODIFIED="1463660260140" TEXT="What about providing a platform for end-users with services based on Fibonacci numbers to do practial things with them?"/>
<node CREATED="1463660264562" ID="ID_1498325519" MODIFIED="1465398045064" TEXT="Database?">
<node CREATED="1463758560332" FOLDED="true" ID="ID_1816810620" MODIFIED="1465398055323" TEXT="Redis - it fulfills all requirements.">
<node CREATED="1463758791619" ID="ID_1420622295" MODIFIED="1463758806889">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Use Redis-Python connector for that - https://redislabs.com/python-redis
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1465398069071" ID="ID_799145911" MODIFIED="1465398074625" TEXT="OpenStack Swift"/>
<node CREATED="1465398075310" ID="ID_1197691459" MODIFIED="1465398084523" TEXT="AWS S3"/>
<node CREATED="1465398085640" ID="ID_794287454" MODIFIED="1465398089691" TEXT="Microsoft Azure"/>
<node CREATED="1465398090290" ID="ID_502788645" MODIFIED="1465398097442" TEXT="SoftLayer"/>
</node>
<node CREATED="1463659867728" FOLDED="true" ID="ID_1535659321" MODIFIED="1465398031925" TEXT="Python/Go">
<node CREATED="1463758544205" ID="ID_502380243" MODIFIED="1463758558316" TEXT="I have decided to go with Python"/>
</node>
<node CREATED="1463660268796" ID="ID_1605620351" MODIFIED="1463660274603" TEXT="Orchestration?"/>
<node CREATED="1463660275263" FOLDED="true" ID="ID_699162817" MODIFIED="1465398030018" TEXT="OpenStack or Apache Mesos?">
<node CREATED="1463758579145" ID="ID_1030635308" MODIFIED="1463758740065">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Probably, that's the overkill, but given the challenge to foresee what will be with this project in 5 years, Apache Mesos seems to be a better fit. We don't want to build a cloud (service) platform, rather we want to be able to harness hardware infrastructure for computations and data storage, therefore Apache Mesos.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1463660533674" ID="ID_1051744801" MODIFIED="1463660542986" TEXT="Bitbucket or Gitlab?"/>
<node CREATED="1463660883777" ID="ID_978920886" MODIFIED="1463661040805">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Is it possible to use multiple cores for calculating Fibonacci numbers?
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1463663217383" ID="ID_1613656333" MODIFIED="1463663232272" TEXT="How many active connections we support?"/>
<node CREATED="1463746318506" FOLDED="true" ID="ID_289423355" MODIFIED="1465398027608" TEXT="Redis vs MongoDb">
<node CREATED="1463746333717" ID="ID_1154107102" MODIFIED="1463746419565" TEXT="Why using Redis?&#xa;&#xa;&quot;TL;DR: If you can map a use case to Redis and discover you aren&apos;t at risk of running out of RAM by using Redis there is a good chance you should probably use Redis. It&apos;s a &quot;NoSQL&quot; key-value data store. More precisely, it is a data structure server.&quot;&#xa;&#xa;http://stackoverflow.com/questions/7888880/what-is-redis-and-what-do-i-use-it-for#7897243&#xa;&#xa;Basically, it fits perfectly what we want to achieve with Fibostore - a key-value storage of Fibonacci numbers."/>
<node CREATED="1464692299475" ID="ID_508162929" MODIFIED="1464692309166">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      How to install and use Redis https://www.digitalocean.com/community/tutorials/how-to-install-and-use-redis
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1463746420868" ID="ID_1910140" MODIFIED="1463746654187" TEXT="Since we are looing into a long-term perspective (5 years), it is safe to assume that the task transforms from computing of Fibonacci numbers rather to store them. What does it? Obviously, we have to architect our system in a way that it would be able to store large datasets (probably, not that large, actually) and provide them to users in a fast manner.&#xa;&#xa;Actually, how big can be a database of Fibonacci numbers? Since they are growing exponentially, I&apos;d assume there aren&apos;t too many of them out there."/>
<node CREATED="1463747173590" ID="ID_528421253" MODIFIED="1464613968035">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      &#1052;&#1086;&#1103; &#1080;&#1076;&#1077;&#1103; &#1089;&#1087;&#1072;&#1091;&#1085;&#1080;&#1090;&#1100; &#1082;&#1086;&#1085;&#1090;&#1077;&#1081;&#1085;&#1077;&#1088;&#1099; &#1085;&#1072; &#1073;&#1072;&#1079;&#1077; &#1082;&#1072;&#1089;&#1090;&#1086;&#1084;&#1085;&#1099;&#1093; &#1080;&#1084;&#1080;&#1076;&#1078;&#1077;&#1081; &#1089; &#1087;&#1088;&#1086;&#1075;&#1088;&#1072;&#1084;&#1084;&#1086;&#1081; &#1074;&#1085;&#1091;&#1090;&#1088;&#1080; &#1076;&#1083;&#1103; &#1087;&#1086;&#1076;&#1089;&#1095;&#1077;&#1090;&#1072; &#1092;&#1080;&#1073;&#1086;&#1085;&#1072;&#1095;&#1095;&#1080;. &#1044;&#1083;&#1103; &#1101;&#1090;&#1086;&#1075;&#1086; &#1090;&#1072;&#1082;&#1078;&#1077; &#1074;&#1086;&#1079;&#1084;&#1086;&#1078;&#1085;&#1086; &#1080;&#1084;&#1077;&#1077;&#1090; &#1089;&#1084;&#1099;&#1089;&#1083; &#1080;&#1084;&#1077;&#1090;&#1100; &#1087;&#1088;&#1080;&#1074;&#1072;&#1090;&#1085;&#1099;&#1081; &#1088;&#1077;&#1087;&#1086; &#1076;&#1083;&#1103; &#1076;&#1086;&#1082;&#1077;&#1088; &#1080;&#1084;&#1080;&#1076;&#1078;&#1077;&#1081;. &#1053;&#1072;&#1089;&#1095;&#1077;&#1090; &#1075;&#1077;&#1085;&#1077;&#1088;&#1072;&#1090;&#1086;&#1088;&#1072; - &#1090;&#1091;&#1090; &#1089;&#1082;&#1086;&#1088;&#1077;&#1081; &#1082;&#1072;&#1082; &#1088;&#1072;&#1079; &#1082;&#1086;&#1088;&#1091;&#1090;&#1080;&#1085;&#1072; &#1087;&#1086;&#1076;&#1086;&#1081;&#1076;&#1077;&#1090;, &#1090;&#1072;&#1082; &#1082;&#1072;&#1082; &#1080;&#1084;&#1077;&#1077;&#1090; &#1089;&#1084;&#1099;&#1089;&#1083; &#1087;&#1077;&#1088;&#1077;&#1076;&#1072;&#1074;&#1072;&#1090;&#1100; &#1074; &#1082;&#1072;&#1095;&#1077;&#1089;&#1090;&#1074;&#1077; &#1074;&#1093;&#1086;&#1076;&#1085;&#1086;&#1075;&#1086; &#1087;&#1072;&#1088;&#1072;&#1084;&#1077;&#1090;&#1088;&#1072; &#1091;&#1078;&#1077; &#1080;&#1079;&#1074;&#1077;&#1089;&#1090;&#1085;&#1086;&#1077; &#1087;&#1086;&#1076;&#1089;&#1095;&#1080;&#1090;&#1072;&#1085;&#1085;&#1086;&#1077; &#1095;&#1080;&#1089;&#1083;&#1086; &#1092;&#1080;&#1073;&#1086;&#1085;&#1072;&#1095;&#1095;&#1080;.
    </p>
    <p>
      &#160;
    </p>
    <p>
      &#1058;&#1086; &#1077;&#1089;&#1090;&#1100;, &#1087;&#1088;&#1080;&#1083;&#1086;&#1078;&#1077;&#1085;&#1080;&#1077; &#1076;&#1083;&#1103; &#1087;&#1086;&#1076;&#1089;&#1095;&#1077;&#1090;&#1072; &#1073;&#1091;&#1076;&#1077;&#1090; &#1091;&#1087;&#1072;&#1082;&#1086;&#1074;&#1072;&#1085;&#1086; &#1074; &#1082;&#1086;&#1085;&#1090;&#1077;&#1081;&#1085;&#1077;&#1088;. &#1079;&#1072;&#1095;&#1077;&#1084; &#1074;&#1086;&#1086;&#1073;&#1097;&#1077; &#1082;&#1086;&#1085;&#1090;&#1077;&#1081;&#1085;&#1077;&#1088;&#1099;? &#1095;&#1090;&#1086;&#1073;&#1099; &#1073;&#1099; (&#1072;) - &#1091;&#1076;&#1086;&#1073;&#1085;&#1086; &#1091;&#1087;&#1088;&#1072;&#1074;&#1083;&#1103;&#1090;&#1100; &#1074;&#1077;&#1088;&#1089;&#1080;&#1086;&#1085;&#1085;&#1086;&#1089;&#1090;&#1100;&#1102; &#1087;&#1088;&#1080;&#1083;&#1086;&#1078;&#1077;&#1085;&#1080;&#1103; &#1087;&#1086;&#1076;&#1089;&#1095;&#1077;&#1090;&#1072; &#1095;&#1077;&#1088;&#1077;&#1079; docker images, (&#1073;) - &#1087;&#1072;&#1088;&#1072;&#1083;&#1083;&#1077;&#1083;&#1080;&#1079;&#1072;&#1094;&#1080;&#1103; &#1080; &#1080;&#1079;&#1086;&#1083;&#1103;&#1094;&#1080;&#1103; &#1079;&#1072;&#1087;&#1088;&#1086;&#1089;&#1086;&#1074; &#1085;&#1072; &#1087;&#1086;&#1076;&#1089;&#1095;&#1077;&#1090;.
    </p>
    <p>
      &#160;
    </p>
    <p>
      &#1050;&#1089;&#1090;&#1072;&#1090;&#1080;, &#1095;&#1077;&#1084; &#1087;&#1083;&#1086;&#1093; &#1074;&#1089;&#1090;&#1088;&#1086;&#1077;&#1085;&#1085;&#1099;&#1081; &#1089;&#1077;&#1088;&#1074;&#1077;&#1088; &#1074;&#1086; flask &#1074; &#1087;&#1088;&#1086;&#1076;&#1072;&#1082;&#1096;&#1077;&#1085;&#1077;? &#1042;&#1086;-&#1087;&#1077;&#1088;&#1074;&#1099;&#1093; &#1101;&#1090;&#1086; &#1083;&#1080;&#1096;&#1072;&#1077;&#1090; &#1085;&#1072;&#1089; &#1075;&#1080;&#1073;&#1082;&#1086;&#1089;&#1090;&#1080; &#1084;&#1072;&#1089;&#1096;&#1090;&#1072;&#1073;&#1080;&#1088;&#1086;&#1074;&#1072;&#1085;&#1080;&#1103; &#1074;&#1093;&#1086;&#1076;&#1103;&#1097;&#1080;&#1093; &#1079;&#1072;&#1087;&#1088;&#1086;&#1089;&#1086;&#1074; (&#1074; &#1089;&#1074;&#1086;&#1077;&#1081; &#1072;&#1088;&#1093;&#1080;&#1090;&#1077;&#1082;&#1090;&#1091;&#1088;&#1077;, &#1084;&#1086;&#1078;&#1085;&#1086; &#1089;&#1087;&#1088;&#1086;&#1077;&#1082;&#1090;&#1080;&#1088;&#1086;&#1074;&#1072;&#1080;&#1090;&#1100; &#1084;&#1085;&#1086;&#1075;&#1086; &#1089;&#1077;&#1088;&#1074;&#1077;&#1088;&#1086;&#1074;, &#1073;&#1072;&#1083;&#1072;&#1085;&#1089;&#1080;&#1088;&#1086;&#1074;&#1097;&#1080;&#1082;&#1080; nginx &#1080;&#1083;&#1080; haproxy), &#1087;&#1083;&#1102;&#1089; &#1074;&#1089;&#1090;&#1088;&#1086;&#1077;&#1085;&#1085;&#1099;&#1081; &#1089;&#1077;&#1088;&#1074;&#1077;&#1088; &#1101;&#1090;&#1086; &#1085;&#1077;&#1089;&#1077;&#1082;&#1091;&#1088;&#1085;&#1086; (&#1084;&#1099; &#1085;&#1077; &#1091;&#1087;&#1088;&#1072;&#1074;&#1083;&#1103;&#1077;&#1084; &#1077;&#1075;&#1086; &#1082;&#1086;&#1085;&#1092;&#1080;&#1075;&#1091;&#1088;&#1072;&#1094;&#1080;&#1077;&#1081; &#1080; &#1079;&#1072;&#1074;&#1080;&#1089;&#1080;&#1084; &#1086;&#1090; &#1089;&#1077;&#1082;&#1091;&#1088;&#1080;&#1090;&#1080; &#1087;&#1072;&#1090;&#1095;&#1077;&#1081; &#1086;&#1090; &#1092;&#1083;&#1072;&#1089;&#1082;&#1072;).<br />
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1463758849541" ID="ID_1922927326" MODIFIED="1464788986913" TEXT="Webserver?">
<node CREATED="1463758853581" ID="ID_653238766" MODIFIED="1463758884143" TEXT="Let&apos;s start with Gunicorn - http://gunicorn.org/#docs. No particular reason, it has a very solid reputation."/>
<node CREATED="1464614376332" ID="ID_897957193" MODIFIED="1464614391017">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      How to setup Flask_Gunicorn+nginx https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-gunicorn-and-nginx-on-ubuntu-14-04
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1464298793665" ID="ID_483827717" MODIFIED="1464298794747" TEXT="https://lincolnloop.com/blog/concurrency-python-vs-go/"/>
<node CREATED="1464614505182" ID="ID_625277018" MODIFIED="1464770034431" TEXT="REST API">
<node CREATED="1464614510910" ID="ID_222552303" MODIFIED="1464614517785" TEXT="Best practices of REST API design http://blog.miguelgrinberg.com/post/designing-a-restful-api-with-python-and-flask"/>
</node>
<node CREATED="1464624426254" ID="ID_1637545975" MODIFIED="1464800768722" TEXT="Caching">
<node CREATED="1464624428697" ID="ID_48433584" MODIFIED="1464624579609">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      We probably do not need to keep all the database of Fibonacci numbers all the time. With the database growing in size this might become a problem. So we need to flush some data to disk. What to flush? We either limit a number of records which are kept in memory and flush them when the limit is exceeded following the principle FIFO.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1464691744802" ID="ID_895181898" MODIFIED="1464691751006" TEXT="Thread safety?"/>
</node>
<node CREATED="1463659776274" ID="ID_1782449665" MODIFIED="1463659778971" POSITION="right" TEXT="Features">
<node CREATED="1463659779658" ID="ID_347839094" MODIFIED="1463659820196">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Calculate Fibonacci numbers for a given 'n'
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1463659821604" ID="ID_885072963" MODIFIED="1463659832917" TEXT="Sync/async requests"/>
</node>
<node CREATED="1463659842740" ID="ID_224316719" MODIFIED="1463660393240" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Deployment model
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1463659846884" ID="ID_342647010" MODIFIED="1463659849692" POSITION="right" TEXT="Architecture">
<node CREATED="1463767764988" ID="ID_1708899684" MODIFIED="1463767769500" TEXT="Client-server"/>
</node>
<node CREATED="1463660488987" ID="ID_296455137" MODIFIED="1463660515208" POSITION="right" TEXT="SW Dev Process/Infrastructure">
<node CREATED="1463660517353" ID="ID_1852458612" MODIFIED="1463660524450" TEXT="Scrum"/>
<node CREATED="1463660525403" ID="ID_449174961" MODIFIED="1463660531498" TEXT="Bitbucket"/>
</node>
<node CREATED="1463656227020" ID="ID_72320920" MODIFIED="1464614425460" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Requirements
    </p>
  </body>
</html></richcontent>
<node CREATED="1463656239230" ID="ID_726342433" MODIFIED="1463767920501">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Scalability
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1463656242116" ID="ID_1546518016" MODIFIED="1463656246469" TEXT="Fault-tolerant"/>
<node CREATED="1463660557732" ID="ID_62563872" MODIFIED="1463660560174" TEXT="REST API"/>
<node CREATED="1463660570589" ID="ID_1671127964" MODIFIED="1463660576085" TEXT="Sync/async requests"/>
</node>
<node CREATED="1463659862909" ID="ID_81551203" MODIFIED="1463659865423" POSITION="right" TEXT="Stack">
<node CREATED="1463767849974" ID="ID_256113334" MODIFIED="1463767898030" TEXT="Linux/Python/Flask/Gunicorn/nginx"/>
<node CREATED="1463767973903" FOLDED="true" ID="ID_6211495" MODIFIED="1464800789695">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      (optional) Linux/Go/Python/Flask/Gunicorn/nginx
    </p>
  </body>
</html></richcontent>
<node CREATED="1463767995626" ID="ID_1323821354" MODIFIED="1463768054632">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      It seems more efficient to use&#160;programming language &quot;closer to bare metal&quot; than Python. Go is a good candidate.
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1463767782425" ID="ID_1851987480" MODIFIED="1463767799119" POSITION="right">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Deployment Model Scenarios
    </p>
  </body>
</html></richcontent>
<node CREATED="1463767799793" ID="ID_1521351529" MODIFIED="1463767820090" TEXT="Single VM"/>
<node CREATED="1463767820850" ID="ID_1128882500" MODIFIED="1463767826457" TEXT="Multiple VMs"/>
<node CREATED="1463767827402" ID="ID_877392148" MODIFIED="1463767830409" TEXT="Docker containers"/>
<node CREATED="1463767832389" ID="ID_868357573" MODIFIED="1463767834185" TEXT="coreos"/>
<node CREATED="1463767842722" ID="ID_581526395" MODIFIED="1463767845144" TEXT="Apache Mesos"/>
</node>
<node CREATED="1464624852934" ID="ID_549261014" MODIFIED="1464693336097" POSITION="left" TEXT="How to test">
<node CREATED="1464624856240" ID="ID_60587098" MODIFIED="1464624857154" TEXT="curl -i http://localhost:5000/fibostore/api/v1.0/10"/>
<node CREATED="1464693337297" FOLDED="true" ID="ID_1679753907" MODIFIED="1465398018148" TEXT="curl options">
<node CREATED="1464693342358" ID="ID_599206908" MODIFIED="1464693393572">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      -X specify HTTP request method e.g. POST
    </p>
    <p>
      -H specify request headers e.g. &quot;Content-type: application/json&quot;
    </p>
    <p>
      -d specify request data e.g. '{&quot;message&quot;:&quot;Hello Data&quot;}'
    </p>
    <p>
      --data-binary specify binary request data e.g. @file.bin
    </p>
    <p>
      -i shows the response headers
    </p>
    <p>
      -u specify username and password e.g. &quot;admin:secret&quot;
    </p>
    <p>
      -v enables verbose mode which outputs info such as request and response headers and errors
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
</map>
