from setuptools import setup, find_packages


with open('README.rst') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='Fibostore',
    version='0.1',
    description='Platform for calculating, storing and retrieving sequences of Fibonacci numbers',
    long_description=readme,
    author='Alexander Klimov',
    author_email='alexander.s.klimov@gmail.com',
    url='https://github.com/kennethreitz/samplemod',
    license=license,
    packages=find_packages(exclude=('tests', 'docs'))
)
