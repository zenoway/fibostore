""" Fibostore service to process incoming requests and respond with Fibonacci sequences
"""

from flask import Flask, jsonify, make_response
import logging

from mathcalc import fibonacci_number
from db import FibodataFactory

app = Flask(__name__)
cache = FibodataFactory.create('redis')  # Only Redis is currently supported as a database back-end


@app.route('/fibostore/api/v1.0/<int:seqlength>', methods=['GET'])
def get_fibonacci_sequence(seqlength):
    """
    This function serves HTTP GET request and returns a sequence of Fibonacci numbers of a given length.

    :param seqlength: Length of Fibonacci sequence
    :return: Fibonacci sequence of a given length in JSON format and HTTP code 200
    """
    logging.info("[fibo-server] Serving the request...")
    return jsonify({'Fibonacci sequence': calc_and_cache(seqlength)}), 200


@app.errorhandler(404)
def not_found(error):
    """
    This function handles 404 error and returns the error in the readable form for humans.

    :param error: Error
    :return: Response in JSON format with comments on the error
    """
    logging.error("[fibo-server] ERROR 404! " + str(error))
    return make_response(jsonify({'error': 'Invalid sequence length or internal error'}), 404)


def calc_and_cache(seqlength):
    """
    This function checks if the request to calculate Fibonacci sequence can be fulfilled without
    computing new Fibonacci numbers.
    If the requested length exceeds the size of current Fibonacci sequence in the database, it will re-use
    existing data to minimize computations. Otherwise, it calculates new numbers, adds them to the database and
    returns updated sequence.

    :param seqlength: Length of Fibonacci sequence
    :return: Fibonacci sequence of a given length
    """
    try:
        fiboseq = cache.get_seq(seqlength)
        if not fiboseq:  # Database is empty, we need to create the first record
            fiboseq = list(fibonacci_number(seqlength))
            cache.add_seq(fiboseq)
        else:  # Database is not empty
            size = len(fiboseq)
            if size < seqlength:
                # Given length exceeds the size of Fibonacci sequence in the database.
                # We need to add new Fibonacci numbers. Use two last numbers to minimize computing.
                penultimate_elem, last_elem = fiboseq[-2:]
                start_num = last_elem + penultimate_elem
                next_num = start_num + last_elem
                fiboseq_delta = list(fibonacci_number(seqlength - size, start_num, next_num))
                cache.add_seq(fiboseq_delta)  # Add newly computed Fibonacci numbers to the database
                fiboseq.extend(fiboseq_delta)
        logging.debug('[fibo-server] Fibonacci sequence to be sent to the user: ' + str(fiboseq))
    except Exception as e:
        logging.error('[fibo-server] ERROR! ERROR! ERROR! ', e)
        fiboseq = []  # Return empty list in this case
    return fiboseq
