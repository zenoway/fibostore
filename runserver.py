#!flask/bin/python

""" This module is an entry point for the application
"""

import logging

from server import app


if __name__ == "__main__":
    # Configure the log
    logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)s %(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S %p')
    logging.info('[fibo-server] Starting fibo-server ...')
    app.run(host='0.0.0.0')
    logging.info('[fibo-server] Stopped.')
