""" This modules provides functionality to work with RedisDB using Python
"""

import redis
import logging

from .fibodata import Fibodata


class RedisProvider(Fibodata):
    """
    Redis DB provider which complies with Fibodata interfaces which is uniform for any provider
    """
    def __init__(self):
        self._db = redis.Redis(host='redis', port=6379, db=0)          # BAD: host is hardcoded!
        self._dbname = 'Fibonacci_Sequence'                            # Database name
        self._db.set_response_callback('LRANGE', self.helper_convert)  # Set response callback for LRANGE command

    def get_seq(self, seqlength):
        """
        Returns the Fibonacci sequence of a given length

        :param seqlength: Sequence length
        :return: Fibonacci numbers or empty list if the database is empty
        """
        logging.info("[redis-db connection] Retrieving the sequence from db ...")
        fiboseq = self._db.lrange(self._dbname, 0, seqlength - 1)
        logging.debug("[redis-db connection] found data matching requested length: " + str(fiboseq))
        return fiboseq

    def add_seq(self, sequence):
        """
        Adds new Fibonacci numbers to the database

        :param fiboseq: Fibonacci numbers
        """
        self._db.rpush(self._dbname, *sequence)

    @staticmethod
    def helper_convert(fiboseq):
        """
        Converts a given sequence to integer

        :param fiboseq: Fibonacci numbers
        """
        return [int(number) for number in fiboseq]
