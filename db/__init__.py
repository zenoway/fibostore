from .redis_provider import RedisProvider


class FibodataFactory(object):
    """
    This is factory class to create database provides (e.g. Redis DB)
    """
    @staticmethod
    def create(dbtype):
        """
        Returns a database provider based on a specified type

        :param dbtype:
        :return:
        """
        if dbtype == 'redis':
            return RedisProvider()
