""" This modules provides class hierarchy for working with data
"""

import abc


class Fibodata(object, metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def get_seq(self, seqlength):
        raise NotImplementedError('implement get_seq() method in child class')

    @abc.abstractmethod
    def add_seq(self, sequence):
        raise NotImplementedError('implement add_seq() method in child class')
