Welcome!
========

**Fibostore** is a platform for calculating and storing Fibonacci numbers. It is designed to be elegant, easy to deploy and scalable service with REST API.  

Licensing
=========
Please see the file called LICENSE.


Contacts
===================
Alexander Klimov, alexander.s.klimov@gmail.com, twitter.com/alex_klimov