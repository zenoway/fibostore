""" This module provides functionality to calculate Fibonacci sequences
"""


def fibonacci_number(length, start_num=0, next_num=1):
    """
    This function yields Fibonacci number until the given length is exceeded.

    :param length: Length of Fibonacci sequence
    :param start_num: Previous to last Fibonacci number (see 'start' parameter below)
    :param next_num: Last Fibonacci number in the sequence
    :return: Fibonacci number
    """
    if length < 0:
        raise ValueError('length is negative')
    a, b = start_num, next_num
    while length > 0:
        yield a
        a, b = b, a + b
        length -= 1
